#! /bin/bash

export JETSON_ROOTFS_DIR=$(pwd)/rootfs
export JETSON_BUILD_DIR=$(pwd)/build

mkdir -p $JETSON_ROOTFS_DIR
mkdir -p $JETSON_BUILD_DIR

./create-rootfs.sh

./create-image.sh